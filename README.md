# EPAM 202.1 Blueprint Intermediate Interfaces& Parent Child Classes

Developed with Unreal Engine 5

## Task Description: 

The Goal of the task is to explore the Parent-Child relations inside the Blueprints. As well as learn about the interfaces that can be linked to Blueprints. 

## Blueprint requirements: 

The following assets should be present in the Project’s Content  Folder: 

1. Blueprint Interface (BPI_ActivatableInterface) 
2. Blueprint Interface (BPI_UsableInterface) 
3. Actor Blueprint (BP_ActivatableActor) that utilizes BPI_ActivatableInterface  
4. Actor Blueprint (BP_UsableActor) that utilizes BPI_UsableInterface 
5. Player Controller Class (BP_InteractionController) that utilizes BPI_UsableInterface 
6. Game Mode (BP_MyGameMode) that utilizes BP_InteractionController 
7. Child BP Actor (BP_Light) (Parented to the BP_ActivatableActor) for the Light  
8. Child BP Actor (BP_LightSwitch) (Parented to the BP_UsableActor) for the Light switch 

## Requirements for assets: 

BP_ActivatableActor: 

- It should be linked to the BPI_ActivatableInterface Blueprint Interface 

BP_UsableActor: 

- It should be linked to the BPI_UsableInterface Blueprint Interface 

BP_InteractionController: 

- Logic that will allow Character to move 

- It uses the BPI_UsableInterface in the Event Graph 

BP_MyGameMode 

- Player Controller Class is set to the BPI_UsableInterface 

BP_Light: 

- Has a Point Light as a Component  

- An event that will control visibility of the Point Light Component 

P_LightSwitch: 

- Has a Static Mesh as a Component 

- An event that will trigger the event that controls the visibility of the Point Light in the BP_Light 

Success criteria: 

- When Pawn is getting near the Light Switch and presses the interact button, a Point Light Actor should become visible 

- Interaction is bound to the keyboard key 

![Result.](/readme/result.png "Result.")

![BP_Controller.](/readme/BP_Controller.PNG "BP_Controller.")

![BP_Light.](/readme/BP_Light.PNG "BP_Light.")

![BP_Switch.](/readme/BP_Switch.PNG "BP_Switch.")